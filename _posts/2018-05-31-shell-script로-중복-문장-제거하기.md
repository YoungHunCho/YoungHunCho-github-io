---
title: "shell script로 중복 문장 제거하기"
categories: 
 - Ubuntu
tags: [shell, 명령어]
---
# shell script로 중복 문장 제거하기
> sort -u input.txt  
or  
> awk '!a[$0]++' input.txt  