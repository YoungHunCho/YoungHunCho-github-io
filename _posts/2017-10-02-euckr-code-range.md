---
layout: post
title: "EUC-KR의 한국어 코드 범위"
categories: 
 - 한글
tags: [한글, euc-kr]
---
# Acsii 범위
0x00 ~ 0x7f

# 완성형 한글 범위 
#### '가'(0xB0A1) ~ '힝'(0xC8FE)
상위 바이트 변이 : 0xB0 ~ 0xC8<br />
하위 바이트 변이 : 0xA1 ~ 0xFE<br />
# 2Byte 특수 문자
상위 바이트 변이 : 0xA1 ~ 0xAC<br />
하위 바이트 변이 : 0xA1 ~ 0xFE<br />
# 자모 코드
자음 30자 : 0xA4A1 ~ 0xA4BE<br />
모음 21자 : 0xA4BF ~ 0xA4D3<br />
옛 한글 자모 : 0xA4D5 ~ 0xA4FE<br />
# 한자 코드
상위 바이트 변이 : 0xCA ~ 0xFD<br />
하위 바이트 변이 : 0xA1 ~ 0xFE<br />