---
title: "파이썬에서 파일 복사"
categories: 
 - Python
tags: [python]
---
# 파이썬에서 파일 복사
파이썬의 shutil모듈을 사용하여서 파일을 복사 할 수 있다.

`copyfile(src, dst)` 함수를 사용하면 된다.

```python
import shutil
copyfile("/home/cho/test.txt", "/home/cho/Desktop/test.txt")
```
`copyfile(src, dst)`

1. 복사될 파일이 있을 경우 덮어쓴다.
2. src가 존재하지 않거나 접근 불가능할때, 그리고 dst가 writable하지 않을 때 IOError가 발생한다.